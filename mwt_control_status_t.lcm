//////////////////////////////////////////////////////////////////////////////
// Copyright 2020  Monterey Bay Aquarium Research Institute                 //
// Distributed under MIT license. See license.txt for more information.     //
//////////////////////////////////////////////////////////////////////////////

package mwt;

// Struct: controller_status_t
// The status of a pcon::pid object.
struct controller_status_t
{
    // Variable: cmd
    // The desired state of the process being controlled.
    double cmd;

    // Variable: measure
    // The measured state of the process being controlled.
    double measure;

    // Variable: integral
    // The current inegrator value from the controller.
    double integral;

    // Variable: derivative
    // The current calculated derivative value from the controller.
    double derivative;

    // Variable: output
    // The current output value from the controller.
    double output;

    // Variable: kp
    // The proportional gain of the controller.
    double kp;

    // Variable: ki
    // The integral gain of the controller.
    double ki;

    // Variable: kd
    // The derivative gain of the controller.
    double kd;

    // Variable: tau
    // A time constant for a bilinear transform filter that is applied to the
    // calculated derivative value.  If the value is zero filtering is
    // disabled.
    double tau;

    // Variable: output_scale
    // A scale factor that is applied to the controller output.  The default
    // value is 1.0.
    double output_scale;
}

// Struct: trajectory_status_t
// The status of a pcon::trajectory object.
struct trajectory_status_t
{
    // Variable: set_point
    // The current set_point value returned by the trajectory object.
    double set_point;

    // Variable: goal
    // The current goal that the trajectory object is moving to.
    double goal;

    // Variable: slew_rate
    // The rate which the trajectory object is moving towards the goal.
    double slew_rate;

    // Variable: is_moving
    // When true the trajectory object is actively moving to the goal,
    // otherwise the set_point equals the goal.
    boolean is_moving;
}

// Struct: filter_status_t
// The status of a bilinear transform filter.
struct filter_status_t
{
    // Variable: input
    // The filter input value.
    double input;

    // Variable: output
    // The filter output value.
    double output;

    // Variable: tau
    // The filter time constant.
    double tau;
}

// Struct: filter_rl_status_t
// The status of a rate-limited bilinear transform filter.
struct filter_rl_status_t
{
    // Variable: input
    // The filter input value.
    double input;

    // Variable: output
    // The filter output value.
    double output;

    // Variable: tau
    // The filter time constant.
    double tau;

    // Variable: max_rate
    // The the maximum rate of change in units/sec allowed.
    double max_rate;
}

// Struct: gain_scale_status_t
// The status of a gain scaling function.
struct gain_scale_status_t
{
    // Variable: enabled
    // When true gain scaling is enabled.
    boolean enabled;

    // Variable: kp_in
    // The proportional gain value into the gain scaling function.
    double kp_in;

    // Variable: ki_in
    // The integral gain value into the gain scaling function.
    double ki_in;

    // Variable: kd_in
    // The derivative gain value into the gain scaling function.
    double kd_in;

    // Variable: kp_out
    // The proportional gain value out of the gain scaling function.
    double kp_out;

    // Variable: ki_out
    // The integral gain value out of the gain scaling function.
    double ki_out;

    // Variable: kd_out
    // The derivative gain value out of the gain scaling function.
    double kd_out;
}

// Struct: mwt_control_status_t
// The status of the mid-water tracking control system.
struct mwt_control_status_t
{
    // Variable: header
    // LCM publisher of this message
    header_t header;

    // Variable: x_control
    // The PID controller status of the x-axis control loop.
    controller_status_t x_control;

    // Variable: y_control
    // The PID controller status of the y-axis control loop.
    controller_status_t y_control;

    // Variable: z_control
    // The PID controller status of the z-axis control loop.
    controller_status_t z_control;

    // Variable: yaw_control
    // The PID controller status of the yaw-axis control loop.
    controller_status_t yaw_control;


    // Variable: x_traj
    // The status of the x-axis trajectory generator.
    trajectory_status_t x_traj;

    // Variable: y_traj
    // The status of the y-axis trajectory generator.
    trajectory_status_t y_traj;

    // Variable: z_traj
    // The status of the z-axis trajectory generator.
    trajectory_status_t z_traj;

    // Variable: yaw_traj
    // The status of the yaw-axis trajectory generator.
    trajectory_status_t yaw_traj;


    // Variable: range_filter
    // The status of the measured range filter.
    filter_rl_status_t range_filter;

    // Variable: heading_filter
    // The status of the measured heading filter.
    filter_status_t heading_filter;

    // Variable: z_target_filter
    // The status of the measured z target offset filter.
    filter_rl_status_t z_target_filter;

    // Variable: bearing_filter
    // The status of the measured bearing filter.
    filter_rl_status_t bearing_filter;

    // Variable: range_gs_filter
    // The status of the measured range filter used by the gain
    // scaling function.
    filter_status_t range_gs_filter;

    // Variable: heading_gs_filter
    // The status of the measured heading filter used by the gain
    // scaling function.
    filter_status_t heading_gs_filter;

    // Variable: bearing_gs_filter
    // The status of the measured bearing filter used by the gain
    // scaling function.
    filter_status_t bearing_gs_filter;


    // Variable: x_effort_filter
    // The status of the x effort filter.
    filter_rl_status_t x_effort_filter;

    // Variable: y_effort_filter
    // The status of the y effort filter.
    filter_rl_status_t y_effort_filter;

    // Variable: z_effort_filter
    // The status of the z effort filter.
    filter_rl_status_t z_effort_filter;

    // Variable: yaw_effort_filter
    // The status of the yaw effort filter.
    filter_rl_status_t yaw_effort_filter;


    // Variable: lateral_gs
    // The status of the lateral gain scaling function.
    gain_scale_status_t lateral_gs;

    // Variable: bearing_gs
    // The status of the bearing gain scaling function.
    gain_scale_status_t bearing_gs;

    // Variable: is_pilot_enabled
    // When true the pilot has control of the Mini ROV.
    boolean is_pilot_enabled;

    // Variable: control_mode
    // Control system modes (1 = IDLE, 2 = SEARCH, 3 = TRACK)
    int32_t control_mode;

    // Variable: is_x_effort_enabled
    // When true effort commands are being sent to the Mini ROV x-axis.
    boolean is_x_effort_enabled;

    // Variable: is_y_effort_enabled
    // When true effort commands are being sent to the Mini ROV y-axis.
    boolean is_y_effort_enabled;

    // Variable: is_z_effort_enabled
    // When true effort commands are being sent to the Mini ROV z-axis.
    boolean is_z_effort_enabled;

    // Variable: is_yaw_effort_enabled
    // When true effort commands are being sent to the Mini ROV yaw-axis.
    boolean is_yaw_effort_enabled;

    // Variable: control_exec_ms
    // The amount of time in milliseconds the control loops are active per
    // time step.
    double control_exec_ms;

    // Variable: other_exec_ms
    // The amount of time in milliseconds the control system spent performing
    // non-control related tasks such as responding to user input.
    double other_exec_ms;

    // Variable: percent_idle
    // The percentage of the time step the control system was waiting for
    // the next time step.  A simple moving average filter is applied to
    // this value.
    double percent_idle;

    // Variable: missed_updates
    // The number of time steps the control system missed an update.
    int64_t missed_updates;

    // Variable: proc_req_max
    // The maximum number of requests processed in the control loop sample
    // period.
    int64_t proc_req_max;

}
