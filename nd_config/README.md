# README #

# [Natural Docs](https://www.naturaldocs.org/) documentation generation config files.


Required software:

+ [Natural Docs](https://www.naturaldocs.org/)

+ [Mono](https://www.mono-project.com/)

+ [GNU Make](https://www.gnu.org/software/make/)

Description:

The Makefile generates html documentation for the LCM types in this repo.  The output from the generation is stored in the repos "docs" directory.  The Mono project and Natural Docs must be installed.  The Makefile requires an alias or script named, "natural_docs" the invokes that Natural Docs utility.