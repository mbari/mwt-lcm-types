##############################################################################
# Copyright 2020  Monterey Bay Aquarium Research Institute                   #
# Distributed under MIT license. See license.txt for more information.       #
##############################################################################

# files and directories
PACKAGE = 		mwt
PACKAGE_JAR = 	lcmtypes_mwt.jar

LCM_JAR = 		/usr/local/share/java/lcm.jar
INSTALL_DIR =	/usr/local/share/java/

# tools

#rules
all: jar_file python

jar_file:
	lcm-gen -j *.lcm
	lcm-gen -j deprecated/*.lcm
	javac -cp $(LCM_JAR) $(PACKAGE)/*.java
	jar cf $(PACKAGE_JAR) $(PACKAGE)/*.class

python:
	lcm-gen -p --ppath . *.lcm
	lcm-gen -p --ppath . deprecated/*.lcm


install:
	sudo cp -i $(PACKAGE_JAR) $(INSTALL_DIR)

.PHONY: clean
clean:
	rm -rf $(PACKAGE) $(PACKAGE_JAR)

	
