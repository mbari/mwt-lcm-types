//////////////////////////////////////////////////////////////////////////////
// Copyright 2020  Monterey Bay Aquarium Research Institute                 //
// Distributed under MIT license. See license.txt for more information.     //
//////////////////////////////////////////////////////////////////////////////

package mwt;

// Struct: ml_cfg_t
// Settings in the ml-boxview GUI
struct ml_cfg_t 
{
    // Variable: score_threshold
    // Boxes with score below this threshold are not displayed
    int32_t score_threshold;

    // Variable: box_deviation
    // Box pairs with deviation larger than this are not combined
    int32_t box_deviation;

    // Variable: reinit_threshold
    // Don't reinit KCF unless both boxes have scores above this
    int32_t reinit_threshold;
    
    // Variable: min_reinit_time
    // Ignore KCF reinit requests that occur in less than this time in ms
    int32_t min_reinit_time;
    
    // Variable: max_epipolar_error
    // Do not use position estimates with error larger than this
    int32_t max_epipolar_error;
    
    // Variable: model_name
    // This is the name of the current model defined in run_ml_low_latency
    string  model_name;
    
    // Variable: target_class
    // The name of the target class current being seached/tracked
    string target_class;
    
    // Variable: tracker_type
    // The name of the visual tracker (KCF, BOOSTING, MOSSE, etc)
    string tracker_type;
    
    // Variable: target_source
    // The name of the box data that provides the 3D target position
    string target_source;
    
    // Variable: box_combiner
    // The method used to find corresponding boxes in boxview
    string box_combiner;
    
    // Variable: box_size_from_ml
    // When true, use the ml box size to drive tracker box size
    int32_t box_size_from_ml;
    
    // Variable: box_padding
    // The extract padding around the ml box size for tracker box
    int32_t box_padding;
    
    // Variable: publish_mwt
    // When true, publish MWT_TARGET messages from boxview
    int32_t publish_mwt;
    
    // Variable: publish_ext
    // When true, publish EXT_TARGET_3 messages from boxview
    int32_t publish_ext;
    
    // Variable: box_size_x
    // The size of the tracker box in x (horz)
    int32_t box_size_x;
    
    // Variable: box_size_y
    // The size of the tracker box in y (vert)
    int32_t box_size_y;

    // Variable: rate_limit
    // A scalar to limit large changes in 3D position
    double rate_limit;
    
    // Variable: min_range
    // The closest range before rejecting 3D position
    double min_range;
    
    // Variable: max_range
    // The largest range before rejecting 3D position
    double max_range;
    
    // Variable: min_alt
    // The smallest altitude before rejecting 3D position
    double min_alt;
    
    // Variable: max_alt
    // The largest altitude before rejecting 3D position
    double max_alt;
}
