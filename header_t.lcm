//////////////////////////////////////////////////////////////////////////////
// Copyright 2020  Monterey Bay Aquarium Research Institute                 //
// Distributed under MIT license. See license.txt for more information.     //
//////////////////////////////////////////////////////////////////////////////

package mwt;

// Struct: header_t
// Used to identify an LCM publisher, typically used as a sub-type.
struct header_t
{
    // Variable: publisher
    // A string that identifies the process publishing the message 
    // ( e.g. hostname:process_name(pid) )
    string publisher;

    // Variable: timestamp
    // A time stamp in seconds since the epoch.
    double timestamp;

    // Variable: sequence
    // A sequence number of published message.
    int64_t sequence;
}
